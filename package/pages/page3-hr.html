<p>Razmotrimo novi primjer. Zadaća nam je modelirati kombinacijski digitalni sklop koji na ulaz dobiva
   4-bitni binarni broj i ima dva izlaza: na izlazu <code>z</code> generira vrijednost <code>'1'</code>
   ako je binarni broj na ulazu jednak 0, a na izlazu <code>w</code> generira vrijednost <code>'1'</code>
   ako je binarni broj na ulazu veći od jedan.</p>

<p>Prvo pitanje oko kojeg se moramo dogovoriti jest: koliko ulaza ima ovaj sklop? Iako naoko trivijalno,
   pitanje ima dva valjana odgovora.</p>

<p><strong>Na fizičkoj razini</strong>, sklop ima 4 ulaza: mora dobiti 4 bita koja zajedno enkapsuliraju 
   vrijednost broja nad kojim sklop treba izračunati rezultat. Kako bitove modeliramo tipom
   <code>std_logic</code>, sučelje ovakvog sklopa (nazovimo ga <em>ispitivalo</em>) mogli bismo modelirati
   na sljedeći način.</p>

<pre><code>ENTITY ispitivalo IS PORT (
  n3, n2, n1, n0: IN std_logic;
  z, w: OUT std_logic
);
END ispitivalo;</code></pre>

<p>Grafički prikaz ovako osmišljenog sučelja prikazan je na slici 1.</p>

<figure class="mlFigure">
<img src="@#file#(ispitivalo1a.svg)" alt="Sučelje sklopa"><br>
<figcaption><span class="mlFigKey">Slika 1.</span> Sučelje sklopa <em>ispitivalo</em>.</figcaption>
</figure>

<p><strong>Na konceptualnoj razini</strong>, sklop ima 1 ulaz: njegov ulaz je 4-bitni binarni broj. Primijetite
   koja je razlika: gledano na ovoj razini, u sklop i dalje možemo dovesti 4 bita, ali izjavom da se radi o 
   jednom ulazu (odnosno činjenici da smo te bitove povezali u grupu) korisniku našeg sklopa šaljemo puno 
   jasniju poruku: tim bitovima dali smo u određenom smislu <em>semantiku</em>.</p>

<p>Upravo za modeliranje ovakvih
   grupa bitova, na raspolaganju nam je tip <code>std_logic_vector</code>. Kada definiramo da je nešto takvog
   tipa, onda moramo navesti i ogradu: koliko bitova imamo u takvoj grupi te kako ih indeksiramo. Konstrukt
   koji se koristi je:<br>
   <code>std_logic_vector(x TO y)</code> ili <code>std_logic_vector(x DOWNTO y)</code>,<br>
   gdje su <code>x</code> i <code>y</code> cijeli brojevi. Ako je <code>x&lt;y</code>, indeksi su rastući
   pa se mora koristiti prvi oblik: <code>std_logic_vector(x TO y)</code>; ako je <code>x&gt;y</code>, 
   indeksi su padajući pa se mora koristiti drugi oblik: <code>std_logic_vector(x DOWNTO y)</code>.
</p>

<p>Oslanjajući se na uporabu vektorskog tipa podataka, definicija sučelja bila bi sljedeća.</p>

<pre><code>ENTITY ispitivalo IS PORT (
  n: IN std_logic_vector(3 DOWNTO 0);
  z, w: OUT std_logic
);
END ispitivalo;</code></pre>

<p>Grafički prikaz ovako osmišljenog sučelja prikazan je na slici 2.</p>

<figure class="mlFigure">
<img src="@#file#(ispitivalo1b.svg)" alt="Sučelje sklopa"><br>
<figcaption><span class="mlFigKey">Slika 2.</span> Bolje sučelje sklopa <em>ispitivalo</em>.</figcaption>
</figure>

<p>U opisu arhitekture sklopa, sada ćemo moći koristiti samo ime <code>n</code> koje označava čitavu grupu
   od četiri bita (što je odlično jer nad takvim grupama možemo primjenjivati ugrađene operatore za posmicanje,
   rotiranje i slično čime već možemo opisivati mnoštvo korisnog digitalnog sklopovlja), podgrupe bitova 
   (primjerice <code>n(3 DOWNTO 2)</code> označava grupu izgrađenu od najljevija dva bita iz grupe <code>n</code>,
   te pojedinačne bitove: <code>n(3)</code>, <code>n(2)</code>, <code>n(1)</code> i <code>n(0)</code> predstavljaju
   redom najljeviji bit iz grupe <code>n</code>, onaj do njega u desno, onaj do njega za dva mjesta u desno te
   onaj najdesniji.</p>

<p>Zamislite na tren da trebate modelirati sklop koji na ulazu dobiva 4-bitni binarni broj a na izlazu generira
   broj koji se dobije njegovom rotacijom za jedno mjesto u lijevo. Ako takvom sklopu pošaljemo na ulaz
   <code>"0011"</code>, očekivali bismo da na izlazu dobijemo <code>"0110"</code>. Evo koliko je jednostavno
   opisati takav sklop.
</p>

<pre><code>library ieee;
use ieee.std_logic_1164.all;

ENTITY vrtalo IS PORT (
  n: IN std_logic_vector(3 DOWNTO 0);
  y: OUT std_logic_vector(3 DOWNTO 0)
);
END vrtalo;

ARCHITECTURE arch OF vrtalo IS
BEGIN
  y &lt;= n rol 1;
END arch;</code></pre>

<p>Umjesto navedenog izraza koji se oslanja na uporabu ugrađenog operatora za rotaciju u lijevo (<code>rol</code>;
   s lijeve strane se navodi grupa bitova a s desne broj mjesta za koliko je potrebno napraviti rotaciju), mogli
   smo koristiti i sljedeće alternative:<br>
   <code>y &lt;= n(2) &amp; n(1) &amp; n(0) &amp; n(3);</code><br>
   ili pak:<br>
   <code>y &lt;= n(2 downto 0) &amp; n(3);</code><br>
   U oba slučaja koristimo operator <code>&amp;</code> koji u VHDL-u spaja (konkatenira) bitove (ili čak grupe) u 
   nove veće grupe. Tako u prvom slučaju kažemo da u <code>y</code> treba pospremiti 4 bita - redom: <code>n(2)</code>,
   <code>n(1)</code>, <code>n(0)</code> i <code>n(3)</code> (skicirajte si na papiru što rotacija radi i uvjerite
   se da je ovo pravilan raspored bitova ako rotiramo za jedno mjesto u lijevo, pri čemu je bit na poziciji 3
   najljeviji jer smo definiciju raspona započeli upravo s time indeksom). U drugom slučaju kažemo da je rezultat
   grupa od četiri bita koja se dobije tako da se podgrupi <code>n(2 downto 0)</code> s desna prilijepi još bit
   <code>n(3)</code>. 
</p>
<p>Dapače, specifikaciju izlaza mogli smo raspisati i izlazni bit po izlazni bit: evo takvog
   rješenja u nastavku.
</p>
<pre><code>y(3) &lt;= n(2);
y(2) &lt;= n(1);
y(1) &lt;= n(0);
y(0) &lt;= n(3);</code></pre>

<p>Vratimo se sada na originalni problem i napišimo prikladan model. Možete li odrediti logičke izraze
   za izlaze <code>z</code> i <code>w</code>? Evo ih u nastavku.<br>
   \( z = \bar n(3) \cdot \bar n(2) \cdot \bar n(1) \cdot \bar n(0) \)<br>
   \( w = \overline{\bar n(3) \cdot \bar n(2) \cdot \bar n(1) \cdot \bar n(0) + \bar n(3) \cdot \bar n(2) \cdot \bar n(1) \cdot n(0)} \)<br>
   pri čemu se ovaj posljednji primjenom teorema simplifikacije može pojednostavniti (za slučaj da taj dio gradiva
   još niste odslušali, nećemo to ovdje napraviti).
</p>

<p>Uz ove logičke izraze, VHDL specifikacija zadanog sklopa prikazana je u nastavku.</p>

<pre><code>library ieee;
use ieee.std_logic_1164.all;

ENTITY ispitivalo IS PORT (
  n: IN std_logic_vector(3 DOWNTO 0);
  z, w: OUT std_logic
);
END ispitivalo;

ARCHITECTURE arch OF ispitivalo IS
BEGIN
  z &lt;= not n(3) and not n(2) and not n(1) and not n(0);
  w &lt;= not( 
       (not n(3) and not n(2) and not n(1) and not n(0)) or 
       (not n(3) and not n(2) and not n(1) and n(0))
    );
END arch;</code></pre>


