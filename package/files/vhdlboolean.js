/* Implementacija case-insensitive stringova... */
function VHDLCIString(str) {
  this.str = str;
  this.val = str.toLowerCase();
  this.vhdlcistring = true;
}

/* toString: */
VHDLCIString.prototype.toString = function() {
  return this.str;
}

/* Usporedba trenutnog stringa s predanim; vraća -1 ako je trenutni manji,
   0 ako je jednak, 1 ako je veći od predanog. 
 */
VHDLCIString.prototype.compareWith = function(other) {
  if(this.val === other.val) return 0;
  if(this.val < other.val) return -1;
  return 1;
}

/* Implementacija polja case-insensitive stringova...
   Naredbe primaju bilo obične stringove (pa ih automatski zamataju) ili
   case-insensitive stringove.
 */
function VHDLCIStringArray() {
  this.arr = [];
  this.vhdlcistringarray = true;
}

/* Stvaranje polja na temelju predanog polja. */
VHDLCIStringArray.fromArray = function(arr) {
  let obj = new VHDLCIStringArray();
  let polje = arr.vhdlcistringarray===undefined ? arr : arr.arr;
  polje.forEach(function(e) {obj.push(e);});
  return obj;
}

/* toString: */
VHDLCIStringArray.prototype.toString = function() {
  return "["+this.arr.join(", ")+"]";
}

/* Sortira trenutno polje elemenata (in-place). */
VHDLCIStringArray.prototype.sort = function() {
  this.arr.sort(function(a,b) {return a.compareWith(b);});
}

/* Uspoređuje dva polja. */
VHDLCIStringArray.prototype.equalsAsArray = function(other) {
  if(other.vhdlcistringarray===undefined) other = VHDLCIStringArray.fromArray(other);
  if(this.arr.length != other.arr.length) return false;
  let n = this.arr.length;
  for(let i = 0; i < n; i++) {
    if(this.arr[i].compareWith(other.arr[i]) != 0) return false;
  }
  return true;
}

/* Vraća novo polje case-insensitive stringova koje je kopija ovog polja. */
VHDLCIStringArray.prototype.copy = function() {
  let narr = new VHDLCIStringArray();
  narr.pushAll(this.arr);
  return narr;
}

/* Uspoređuje dva polja. */
VHDLCIStringArray.prototype.removeAll = function(other) {
  if(other.vhdlcistringarray===undefined) other = VHDLCIStringArray.fromArray(other);
  for(let i = 0; i < other.arr.length; i++) {
    let index = this.indexOf(other.arr[i]);
    if(index!=-1) {
      this.removeFromPosition(index);
    }
  }
  return this;
}

/* Briše iz polja element sa zadane pozicije. */
VHDLCIStringArray.prototype.removeFromPosition = function(pos) {
  this.arr.splice(pos, 1);
}

/* Briše iz polja zadani element ako takav postoji (njegovu prvu pojavu). */
VHDLCIStringArray.prototype.remove = function(elem) {
  let index = this.indexOf(elem);
  if(index != -1) this.removeFromPosition(index);
}

/* U polje dodaje predano ime. Vraća poziciju na koju je ime umetnuto u polje. */
VHDLCIStringArray.prototype.push = function(v) {
  let val = v.vhdlcistring===undefined ? new VHDLCIString(v) : v;
  this.arr.push(val);
  return this.arr.length-1;
}

/* U polje dodaje sve elemente primljenog polja. */
VHDLCIStringArray.prototype.pushAll = function(arr) {
  if(arr.vhdlcistringarray===undefined) arr = VHDLCIStringArray.fromArray(arr);
  for(let i = 0; i < arr.arr.length; i++) {
    this.push(arr.arr[i]);
  }
}

/* U polje dodaje samo one elemente iz primljenog polja koji još nisu prisutni. */
VHDLCIStringArray.prototype.pushAllNotPresent = function(arr) {
  if(arr.vhdlcistringarray===undefined) arr = VHDLCIStringArray.fromArray(arr);
  for(let i = 0; i < arr.arr.length; i++) {
    this.pushIfNotPresent(arr.arr[i]);
  }
}

/* U polje dodaje predano ime, ako ono ne postoji. Vraća poziciju na koju je ime umetnuto
   u polje (ako je umetnuto) odnosno poziciju na kojoj se ime već nalazi, ako je prisutno u polju. */
VHDLCIStringArray.prototype.pushIfNotPresent = function(v) {
  let val = v.vhdlcistring===undefined ? new VHDLCIString(v) : v;
  let index = this.indexOf(val);
  if(index === -1) index = this.push(val);
  return index;
}

VHDLCIStringArray.prototype.contains = function(v) {
  return this.indexOf(v) != -1;
}

VHDLCIStringArray.prototype.size = function() {
  return this.arr.length;
}

VHDLCIStringArray.prototype.indexOf = function(v) {
  let val = v.vhdlcistring===undefined ? new VHDLCIString(v) : v;
  for(let i = 0; i < this.arr.length; i++) {
    if(this.arr[i].compareWith(val) === 0) return i;
  }
  return -1;
}

/* Implementacija posjetitelja kojeg se može pozivati nad Expression dijelom stabla 
   koje generira VHDL parser, a čija je zadaća ekstrakcija imena varijabli, konstanti
   te operatora koji se spominju u izrazu.
 */
function ExpressionInfoExtractor() {
  this.usedNames = new VHDLCIStringArray();
  this.usedBinaryOperators = [];
  this.usedUnaryOperators = [];

  this.visitSimpleName = function(node) {
    this.usedNames.pushIfNotPresent(node.name);
  } // VHDLTNSimpleName
  this.visitIndexedName = function(node) {
    let nm = node.name + "(" + node.index + ")";
    this.usedNames.pushIfNotPresent(nm);
  } // VHDLTNIndexedName
  this.visitBinaryLogicalOperator = function(node) {
    if(this.usedBinaryOperators.indexOf(node.operName)==-1) this.usedBinaryOperators.push(node.operName);
    for(let i = 0; i < node.children.length; i++) {
      node.children[i].accept(this);
    }
  } // VHDLBinaryLogicalOperator
  this.visitUnaryOperator = function(node) {
    if(this.usedUnaryOperators.indexOf(node.operName)==-1) this.usedUnaryOperators.push(node.operName);
    node.child.accept(this);
  } // VHDLUnaryOperator
  this.visitDefault = function(node) {
    throw {name: "ExpressionInfoExtractorException", message: "Pronađeni nedopušteni konstrukti."};
  } // Default case
}

/* Implementacija posjetitelja kojeg se može pozivati nad Expression dijelom stabla 
   koje generira VHDL parser, a čija je zadaća generiranje stabla Booleove funkcije.
   Prvi argument mora biti objekt tipa VHDLCIStringArray koji čuva imena varijabli koje
   se prihvaćaju. Argument addIfMissing regulira ako se pronađe ime koje nije u polju,
   što napraviti: ako je true, ime će biti dodano u polje; ako je false, bacit će se
   iznimka.
 */
function BooleanFunctionBuilder(varNames, addIfMissing) {
  this.exprStack = [];

  function arrayCopy(arr) {
    return arr.map(function(e) {return e.copy();});
  }

  function bfVariable(index) {
    this.index = index;
    this.accept = function(v) { v.visitVariable(this); }
    this.copy = function() { return new bfVariable(this.index); }
  }
  function bfConstant(c) {
    if(!(c==='1' || c==='0')) throw "Korištena nepodržana konstanta.";
    this.c = (c==='0') ? 0 : 1;
    this.accept = function(v) { v.visitConstant(this); }
    this.copy = function() { return new bfConstant(this.c); }
  }
  function bfNOT(expression) {
    this.expression = expression;
    this.accept = function(v) { v.visitNOT(this); }
    this.copy = function() { return new bfNOT(this.expression.copy()); }
  }
  function bfAND(expressions) {
    this.expressions = expressions;
    this.accept = function(v) { v.visitAND(this); }
    this.copy = function() { return new bfAND(arrayCopy(this.expression)); }
  }
  function bfOR(expressions) {
    this.expressions = expressions;
    this.accept = function(v) { v.visitOR(this); }
    this.copy = function() { return new bfOR(arrayCopy(this.expression)); }
  }
  function bfNAND(expressions) {
    this.expressions = expressions;
    this.accept = function(v) { v.visitNAND(this); }
    this.copy = function() { return new bfNAND(arrayCopy(this.expression)); }
  }
  function bfNOR(expressions) {
    this.expressions = expressions;
    this.accept = function(v) { v.visitNOR(this); }
    this.copy = function() { return new bfNOR(arrayCopy(this.expression)); }
  }
  function bfXOR(expressions) {
    this.expressions = expressions;
    this.accept = function(v) { v.visitXOR(this); }
    this.copy = function() { return new bfXOR(arrayCopy(this.expression)); }
  }
  function bfXNOR(expressions) {
    this.expressions = expressions;
    this.accept = function(v) { v.visitXNOR(this); }
    this.copy = function() { return new bfXNOR(arrayCopy(this.expression)); }
  }

  this.visitSimpleName = function(node) {
    let nm = new VHDLCIString(node.name);
    let index = varNames.pushIfNotPresent(nm);
    this.exprStack.push(new bfVariable(index));
  } // VHDLTNSimpleName
  this.visitIndexedName = function(node) {
    let nm = new VHDLCIString(node.name + "(" + node.index + ")");
    let index = varNames.pushIfNotPresent(nm);
    this.exprStack.push(new bfVariable(index));
  } // VHDLTNIndexedName
  this.visitBinaryLogicalOperator = function(node) {
    var exprs = [];
    for(let i = 0; i < node.children.length; i++) {
      node.children[i].accept(this);
      exprs.push(this.exprStack.pop());
    }
    switch(node.operName) {
      case 'and': this.exprStack.push(new bfAND(exprs)); break;
      case 'or': this.exprStack.push(new bfOR(exprs)); break;
      case 'nand': this.exprStack.push(new bfNAND(exprs)); break;
      case 'nor': this.exprStack.push(new bfNOR(exprs)); break;
      case 'xor': this.exprStack.push(new bfXOR(exprs)); break;
      case 'xnor': this.exprStack.push(new bfXNOR(exprs)); break;
      default:
        throw {name: "BooleanFunctionBuilderException", message: "Pronađena uporaba nedopuštenog operatora: '"+node.operName+"'."};
    }
  } // VHDLBinaryLogicalOperator
  this.visitUnaryOperator = function(node) {
    node.child.accept(this);
    switch(node.operName) {
      case 'not': this.exprStack.push(new bfNOT(this.exprStack.pop())); break;
      default:
        throw {name: "BooleanFunctionBuilderException", message: "Pronađena uporaba nedopuštenog operatora: '"+node.operName+"'."};
    }
  } // VHDLUnaryOperator
  this.visitDefault = function(node) {
    throw {name: "BooleanFunctionBuilderException", message: "Pronađeni nedopušteni konstrukti."};
  } // Default case
}

function BFCalcExpressionVisitor(values) {
  this.stack = [];
  this.visitVariable = function(node) {
    this.stack.push(values[node.index]);
  } // index
  this.visitConstant = function(node) {
    this.stack.push(node.c);
  } // c
  this.visitNOT = function(node) {
    node.expression.accept(this);
    this.stack.push(this.stack.pop() === 0 ? 1 : 0);
  } // expression
  this.visitAND = function(node) {
    for(let i = 0; i < node.expressions.length; i++) {
      node.expressions[i].accept(this);
      if(this.stack.pop()===0) { this.stack.push(0); return; }
    }
    this.stack.push(1);
  } // expressions
  this.visitOR = function(node) {
    for(let i = 0; i < node.expressions.length; i++) {
      node.expressions[i].accept(this);
      if(this.stack.pop()===1) { this.stack.push(1); return; }
    }
    this.stack.push(0);
  } // expressions
  this.visitNAND = function(node) {
    for(let i = 0; i < node.expressions.length; i++) {
      node.expressions[i].accept(this);
      if(this.stack.pop()===0) { this.stack.push(1); return; }
    }
    this.stack.push(0);
  } // expressions
  this.visitNOR = function(node) {
    for(let i = 0; i < node.expressions.length; i++) {
      node.expressions[i].accept(this);
      if(this.stack.pop()===1) { this.stack.push(0); return; }
    }
    this.stack.push(1);
  } // expressions
  this.visitXOR = function(node) {
    let res = 0;
    for(let i = 0; i < node.expressions.length; i++) {
      node.expressions[i].accept(this);
      if(this.stack.pop()===1) res = res===0 ? 1 : 0;
    }
    this.stack.push(res);
  } // expressions
  this.visitXNOR = function(node) {
    let res = 1;
    for(let i = 0; i < node.expressions.length; i++) {
      node.expressions[i].accept(this);
      if(this.stack.pop()===1) res = res===0 ? 1 : 0;
    }
    this.stack.push(res);
  } // expressions
}

